<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Gka0_[4[Z@~iW}~ka-DU+6cva/,G)~@y8bgcP#-&U:uL]l?/5kHR|r|6+)|ng0:5' );
define( 'SECURE_AUTH_KEY',  'ihMk|xks$L*>d!(+^F?Gjvw7PxgOjhR*f1ybN{hIaiy1.0Ojtf4]ZS5/k(Y+SiN_' );
define( 'LOGGED_IN_KEY',    '_ke%R^KP|W4DEa=;=]E MK_P9gYc`_#h0dbs$PUU|C`0}56M`RQ};T``e#[fW(;A' );
define( 'NONCE_KEY',        'J_m*(=a):v&D#g~lP1jhWz:w@4wf^-&%LKf?ZcXt(m%Bs0>6BK<Tj](9~cMX3RtP' );
define( 'AUTH_SALT',        '-?US2bj]uf<pTOVtr/`&x(+]P$9hyy.c>uabuEyHaX$cj2HT)oSyDfMt!FZ%fLH:' );
define( 'SECURE_AUTH_SALT', '8)SChg8iVCwZYm:b%)s4._4ad mLK>6?M,C@Rlk7{>><3=)SB*R]vi}?FRMDe{1K' );
define( 'LOGGED_IN_SALT',   'o[JH&NjAp)N{aewfCHd4hw}kg}N`7LjVd~L(fM[C,(>Z0dDG:[QfD*lVuf}Y.t7b' );
define( 'NONCE_SALT',       '`xHeB[VC/ +3%`$nmQe]@:s9zv}i!bL4&fj2-Nl|b=S*tP,}XJf{^mRw1_Ls8Vb&' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define( 'FS_METHOD', 'direct');